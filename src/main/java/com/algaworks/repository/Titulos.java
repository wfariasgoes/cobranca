package com.algaworks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.algaworks.model.Titulo;

public interface Titulos extends JpaRepository<Titulo, Long>{

}
