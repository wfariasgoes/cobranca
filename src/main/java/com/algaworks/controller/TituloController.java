package com.algaworks.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.algaworks.model.StatusTitulo;
import com.algaworks.model.Titulo;
import com.algaworks.repository.Titulos;
import com.algaworks.service.CadastroTituloService;

@Controller
@RequestMapping("/titulos")
public class TituloController {
	private static final String CADASTRO_VIEW = "CadastroTitulo";
	@Autowired
	private Titulos titulos;

	@Autowired 
	private CadastroTituloService cadastroTituloService;
	
	@RequestMapping("/novo")
	public ModelAndView novo() {
		ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
		mv.addObject(new Titulo());
		return mv;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String salvar(@Validated Titulo titulo, Errors erros, RedirectAttributes atributes) {
		if (erros.hasErrors()) {
			return CADASTRO_VIEW;
		}
		
		try{
			cadastroTituloService.salvar(titulo);
			atributes.addFlashAttribute("mensagem", "Dízimo salvo com sucesso!");
			return "redirect:/titulos/novo";
		}catch (IllegalArgumentException e) {
			erros.rejectValue("dataVencimento", null,e.getMessage());
			return CADASTRO_VIEW;
		}  
	}

	// Metodo para listar todos os títulos, já está mapeado no início da classe
	// como ("/titulos")
	@RequestMapping()
	public ModelAndView pesquisar(Pageable pageable) {
		List<Titulo> todosTitulos = titulos.findAll(); // FindAll retorna a
														// lista de titulos
		ModelAndView mv = new ModelAndView("PesquisaTitulos");
		mv.addObject("titulos", todosTitulos);
		return mv;
	}

	@RequestMapping("{codigo}")
	public ModelAndView editar(@PathVariable("codigo") Titulo titulo) {
		// Titulo titulo = titulos.findOne(codigo); //Recupera o título do id
		// escolhido : Não precisa mais dessa linha, o findAll é feito
		// implicitamente pelo PathVariable
		ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
		mv.addObject(titulo);
		return mv;
	}

	@RequestMapping(value = "{codigo}", method = RequestMethod.DELETE)
	public String excluir(@PathVariable Long codigo, RedirectAttributes attributes) {
		cadastroTituloService.excluir(codigo); 
		attributes.addFlashAttribute("mensagem", "Dizimo excluído com sucesso!");
		return "redirect:/titulos";
	}

	// método utilizado para listar os status
	@ModelAttribute("todosStatusTitulo")
	public List<StatusTitulo> todosStatusTitulo() {
		return Arrays.asList(StatusTitulo.values());
	}

}
